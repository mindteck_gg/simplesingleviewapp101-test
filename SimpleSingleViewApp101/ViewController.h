//
//  ViewController.h
//  SimpleSingleViewApp101
//
//  Created by Gaurav Gupta on 03/10/15.
//  Copyright © 2015 Yow2br. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *bugfixNumber;

@end

