//
//  SimpleControls.m
//  SimpleSingleViewApp101
//
//  Created by Gaurav Gupta on 09/10/15.
//  Copyright © 2015 Yow2br. All rights reserved.
//

#import "SimpleControls.h"

@interface SimpleControls () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UISwitch *swchControl;
- (IBAction)switchOnLabelLight:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lightLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapToflip;

- (IBAction)tappingLableFlipsSwitcj:(id)sender;

@end

@implementation SimpleControls

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  _lightLabel.backgroundColor = [UIColor blackColor];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)switchOnLabelLight:(id)sender {
  if ([_swchControl isOn]) {
    _lightLabel.backgroundColor = [UIColor yellowColor];
  }
    else{
      _lightLabel.backgroundColor = [UIColor redColor];
    }
  }

- (IBAction)tappingLableFlipsSwitcj:(id)sender {
    //  BOOL state = [_swchControl state];

  if ([_swchControl isOn]) {
    [_swchControl setOn:FALSE animated:YES];
  }
  else
    [_swchControl setOn:TRUE animated:YES];
}


@end
